package experiments;

import java.io.FileWriter;
import java.io.IOException;

import parallel.ABCParallel;
import parallel.BestSolution;
import abc.ABC;
import abc.Data;
import abc.Solution;

public class ParallelExperiment {
	


	private static String datapath = "../mdgplib/";
	private static String[] setNames = {"Geo", "RanInt", "RanReal"};
	private static String[] testNs = {"012", "030", "060", "120"};
	private static String[] testNbs = {"01","02","03","04","05","06","07","08","09","10"};
	private static String[] problems = {"ss", "ds"};
	
	private static int NP = 20;
	private static double pls = 1;
	
	private FileWriter fileWriter;
	
	private ParallelExperiment() {
		try {
			fileWriter = new FileWriter("results-par.csv");
			fileWriter.append("filename,n,m,ds/ss,timelimit,parallel,fitness\n");
//			fileWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void writeResult(String filename, Data data, double tmax, boolean parallel, double fitness, String problem){
		try {
			String result = filename + ",";
			result += data.getNbElements() + "," + data.getNbGroups() + "," + problem + ",";
			result += tmax + "," + parallel + "," + fitness;
			fileWriter.write(result + "\n");
//			fileWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void closeWriter() {
		try {
			fileWriter.flush();
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		run();
	}
	
	public static void run() {
		ParallelExperiment experiments = new ParallelExperiment();
		for (String set : setNames) {
			experiments.testSet(set);
		}
		experiments.closeWriter();
	}

	private void testSet(String set) {
		for (String n : testNs) {
			for (String i : testNbs) {
				for (String p : problems) {
					String test = set + "_n" + n + "_" + p + "_" + i + ".txt";
					try {
						testFile(test, set, p);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	private String getFilePath(String set, String test) {
		return datapath + "/" + set + "/"+ test;
	}

	private void testFile(String file, String set, String problem) throws IOException {
		Data data = new Data(getFilePath(set, file));
		System.out.println("testing " + file);
		double tmax = getTime(data.getNbElements());
		try {
			ABC alg = new ABC(data, NP, pls, tmax, 1, 0.1f, true, true, true);
			Solution solution = alg.exec();
			writeResult(file, data, tmax, false, solution.getFitnessValue(data), problem);
			ABCParallel algp = new ABCParallel(data, NP, pls, (long) tmax, 1, 0.1f, true, true, true);
			BestSolution solutionp = algp.exec();
			writeResult(file, data, tmax, true, solutionp.getFitness(), problem);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private double getTime(int n) {
		if (n == 12)
			return 1;
		if (n == 30)
			return 2;
		if (n == 60)
			return 4;
		if (n == 120)
			return 15;
		return 0;
	}	
}
