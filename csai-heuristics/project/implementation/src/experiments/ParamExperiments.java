package experiments;

import java.io.FileWriter;
import java.io.IOException;

import abc.ABC;
import abc.Data;
import abc.Solution;

public class ParamExperiments {
	
	private static String datapath = "../mdgplib/";
	private static String[] setNames = {"Geo", "RanInt", "RanReal"};
	private static String[] testNs = {"010", "012", "030", "060", "120"};
	private static String[] testNbs = {"01","02","03","04","05","06","07","08","09","10"};
	private static String[] problems = {"ss", "ds"};
	
	private static int NP = 20;
	private static double pls = 1;
	private static float[] limits = {0.5f,1f,2f};
	private static float[] nds = {0.1f,0.2f,0.3f,0.4f,0.5f};
	
	private FileWriter fileWriter;
	
	private ParamExperiments() {
		try {
			fileWriter = new FileWriter("results-param.csv");
			fileWriter.append("filename,n,m,ds/ss,timelimit,limit,nd,fitness\n");
//			fileWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeResult(String filename, Data data, double tmax, float limit, float nd, double fitness, String problem){
		try {
			String result = filename + ",";
			result += data.getNbElements() + "," + data.getNbGroups() + "," + problem + ",";
			result += tmax + "," + limit + "n," + nd + "n," + fitness;
			fileWriter.write(result + "\n");
//			fileWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void closeWriter() {
		try {
			fileWriter.flush();
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		ParamExperiments experiments = new ParamExperiments();
		for (String set : setNames) {
			experiments.testSet(set);
		}
		experiments.closeWriter();
	}

	private void testSet(String set) {
		for (String n : testNs) {
			for (String i : testNbs) {
				for (String p : problems) {
					String test = set + "_n" + n + "_" + p + "_" + i + ".txt";
					try {
						testFile(test, set, p);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	private String getFilePath(String set, String test) {
		return datapath + "/" + set + "/"+ test;
	}

	private void testFile(String file, String set, String problem) throws IOException {
		Data data = new Data(getFilePath(set, file));
		System.out.println("testing " + file);
		double tmax = getTime(data.getNbElements());
		for (float limit_factor : limits) {
			for (float nd_factor : nds) {
				try {
					ABC alg = new ABC(data, NP, pls, tmax, limit_factor, nd_factor, true, true, true);
					Solution solution = alg.exec();
					writeResult(file, data, tmax, limit_factor, nd_factor, solution.getFitnessValue(data), problem);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
/*
	private double getTime(int n) {
		if (n <= 12)
			return 1;
		if (n == 30)
			return 2;
		if (n == 60)
			return 4;
		if (n == 120)
			return 20;
		if (n == 240)
			return 40;
		if (n == 480)
			return 120;
		if (n == 960)
			return 600;
		return 0;
	}
	*/
	private double getTime(int n) {
		if (n <= 60)
			return 1;
		if (n == 120)
			return 3;
		if (n == 240)
			return 20;
		if (n == 480)
			return 120;
		if (n == 960)
			return 600;
		return 0;
	}

}