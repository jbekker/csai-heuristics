package experiments;

import java.io.FileWriter;
import java.io.IOException;

import abc.ABC;
import abc.Data;
import abc.Solution;

public class NOExperiment {
	


	private static String datapath = "../mdgplib/";
	private static String[] setNames = {"Geo", "RanInt", "RanReal"};
	private static String[] testNs = {"012", "030", "060", "120"};
	private static String[] testNbs = {"01","02","03","04","05","06","07","08","09","10"};
	private static String[] problems = {"ss", "ds"};
	
	private static int NP = 20;
	private static double pls = 1;
	private static boolean[] NO1 = {true, false};
	private static boolean[] NO2 = {true, false};
	private static boolean[] NO3 = {true, false};
	
	private FileWriter fileWriter;
	
	private NOExperiment() {
		try {
			fileWriter = new FileWriter("results-no.csv");
			fileWriter.append("filename,n,m,ds/ss,timelimit,NO1,NO2,NO3,fitness\n");
//			fileWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void writeResult(String filename, Data data, double tmax, boolean NO1, boolean NO2, boolean NO3, double fitness, String problem){
		try {
			String result = filename + ",";
			result += data.getNbElements() + "," + data.getNbGroups() + "," + problem + ",";
			result += tmax + "," + NO1 + "," + NO2 + "," + NO3 + "," + fitness;
			fileWriter.write(result + "\n");
//			fileWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void closeWriter() {
		try {
			fileWriter.flush();
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		run();
	}
	
	public static void run(){
		NOExperiment experiments = new NOExperiment();
		for (String set : setNames) {
			experiments.testSet(set);
		}
		experiments.closeWriter();
	}

	private void testSet(String set) {
		for (String n : testNs) {
			for (String i : testNbs) {
				for (String p : problems) {
					String test = set + "_n" + n + "_" + p + "_" + i + ".txt";
					try {
						testFile(test, set, p);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	private String getFilePath(String set, String test) {
		return datapath + "/" + set + "/"+ test;
	}

	private void testFile(String file, String set, String problem) throws IOException {
		Data data = new Data(getFilePath(set, file));
		System.out.println("testing " + file);
		double tmax = getTime(data.getNbElements());
		for (boolean no1 : NO1) {
			for (boolean no2 : NO2) {
				for (boolean no3 : NO3) {
					if(no1 || no2 || no3){
						try {
							ABC alg = new ABC(data, NP, pls, tmax, 1, 0.1f, no1, no2, no3);
							Solution solution = alg.exec();
							writeResult(file, data, tmax, no1, no2, no3, solution.getFitnessValue(data), problem);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	private double getTime(int n) {
		if (n == 12)
			return 1;
		if (n == 30)
			return 2;
		if (n == 60)
			return 4;
		if (n == 120)
			return 15;
		return 0;
	}	
}
