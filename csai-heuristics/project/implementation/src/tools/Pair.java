package tools;

public class Pair<T1,T2> {
	
	
	public Pair(T1 left, T2 right) {
		this.left = left;
		this.right = right;
	}
	
	public T1 left;
	public T2 right;
	
	@Override
	public String toString() {
		return "{"+left.toString()+","+right.toString()+"}";
	}

}
