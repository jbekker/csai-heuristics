package test;

import java.io.IOException;

import abc.ABC;
import abc.Data;
import abc.Solution;

public class MainAlgoTest {

	private static String path = "../mdgplib/Geo/Geo_n240_ds_01.txt";
	
	public static void main(String[] args) throws IOException, InterruptedException {
		
		Data data = new Data(path);
		
		ABC abc = new ABC(data, 20, 0.7,/*time*/ 20, 1, 0.1f, true, true, true);
		Solution sol = abc.exec();
		
		System.out.println(sol);
		System.out.println(sol.getFitnessValue(data));
		
	}

}
