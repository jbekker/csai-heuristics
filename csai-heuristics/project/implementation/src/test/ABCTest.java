package test;
import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import abc.ConstructSolution;
import abc.Data;
import abc.Solution;


public class ABCTest {
	
	private static String path = "../mdgplib/RanInt/RanInt_n010_ss_06.txt";
	
	private static Data data;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		data = new Data(path);
	}

	@Test
	public void testConstructSolution() {
		Solution S = new ConstructSolution(data).exec();
		System.out.println(S.toString());
		assertTrue(S.isCompleteSolution(data));
	}
	
	@Test
	public void test() throws Exception {
		
	}

}
