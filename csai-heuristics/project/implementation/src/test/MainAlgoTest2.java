package test;

import java.io.IOException;

import parallel.ABCParallel;
import parallel.BestSolution;
import abc.Data;

public class MainAlgoTest2 {

	private static String path = "../mdgplib/Geo/Geo_n240_ds_01.txt";
	
	public static void main(String[] args) throws IOException, InterruptedException {
		
		Data data = new Data(path);
		
		ABCParallel abc = new ABCParallel(data, 20, 0.7, 30, 1, 0.1f, true, true, true);
		BestSolution sol = abc.exec();
		System.out.println(sol.getSolution());
		System.out.println(sol.getFitness());
		
	}

}
