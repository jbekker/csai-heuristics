package test;

import java.io.IOException;

import abc.Data;
import abc.ILocalImprovement;
import abc.LocalImprovement;
import abc.Solution;

public class LocalImprovementTest {

	
	private static String path = "../mdgplib/Geo/Geo_n010_ds_01.txt";
	
	
	public static void main(String[] args) throws IOException {
		
		Data data = new Data(path);
		
		Solution sol = new Solution(2);
		sol = sol.addElementTo(0,0, data);
		sol = sol.addElementTo(1,0, data);
		sol = sol.addElementTo(2,0, data);
		sol = sol.addElementTo(3,0, data);
		sol = sol.addElementTo(4,0, data);
		sol = sol.addElementTo(5,1, data);
		sol = sol.addElementTo(6,1, data);
		sol = sol.addElementTo(7,1, data);
		sol = sol.addElementTo(8,1, data);
		sol = sol.addElementTo(9,1, data);
		
		ILocalImprovement improver = new LocalImprovement(data);
		
		sol = improver.exec(sol);
		
		System.out.println(sol);
	}

}
