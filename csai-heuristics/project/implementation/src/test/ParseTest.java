package test;

import java.io.IOException;

import abc.Data;



public class ParseTest {

	private static String path = "../mdgplib/Geo/Geo_n010_ds_01.txt";
	
	public static void main(String[] args) throws IOException {
		Data data = new Data(path);
		System.out.println(data.getNbElements() + "\n");
		System.out.println(data.getNbGroups() + "\n");
		System.out.println(data.getDissimilarity(3, 7)+ "\n");
		System.out.println(data.getDissimilarity(7, 3)+ "\n");
		System.out.println(data.getLowerBound(0) + "\n");
		System.out.println(data.getLowerBound(1) + "\n");
		System.out.println(data.getUpperBound(0) + "\n");
		System.out.println(data.getUpperBound(1) + "\n");
		System.out.println(data.getDissimilarity(3, 3)+ "\n");
	}

}
