package test;

import java.io.IOException;

import abc.ConstructSolution;
import abc.Data;
import abc.IGenerateNeighbouring;
import abc.NO1;
import abc.NO2;
import abc.NO3;
import abc.Solution;

public class GenerateNeighbouringTest {

	private static String path = "../mdgplib/Geo/Geo_n010_ds_01.txt";
	
	
	public static void main(String[] args) throws IOException {
		
		Data data = new Data(path);
		
		Solution sol = new Solution(2);
		sol = sol.addElementTo(0,0, data);
		sol = sol.addElementTo(1,0, data);
		sol = sol.addElementTo(2,0, data);
		sol = sol.addElementTo(3,0, data);
		sol = sol.addElementTo(4,0, data);
		sol = sol.addElementTo(5,1, data);
		sol = sol.addElementTo(6,1, data);
		sol = sol.addElementTo(7,1, data);
		sol = sol.addElementTo(8,1, data);
		sol = sol.addElementTo(9,1, data);
		sol = new ConstructSolution(data).exec();

		IGenerateNeighbouring no1 = new NO1(data,3);
		IGenerateNeighbouring no2 = new NO2(data,3);
		IGenerateNeighbouring no3 = new NO3(data, 3);

		
		System.out.println(sol);
		Solution sol1 = no1.exec(sol);
		System.out.println(sol1);
		Solution sol2 = no2.exec(sol);
		System.out.println(sol2);
		Solution sol3 = no3.exec(sol);
		System.out.println(sol3);
		
	}
	
}
