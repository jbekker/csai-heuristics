package parallel;

import java.util.Random;

import abc.ConstructSolution;
import abc.Data;
import abc.IConstructSolution;
import abc.IGenerateNeighbouring;
import abc.ILocalImprovement;
import abc.LocalImprovement;
import abc.NO;
import abc.Solution;

public class ABCParallel {
	
	public ABCParallel(Data data, int NP, double pls, long tmax, float limit_factor, float nd_factor, boolean NO1, boolean NO2, boolean NO3) {
		this.data = data;
		this.NP=NP;
		this.pls=pls;
		this.tmax=1000*tmax;
		this.limit=Math.round(limit_factor*data.getNbElements());
		this.nd=Math.round(nd_factor*data.getNbElements());
		this.NO1=NO1;
		this.NO2=NO2;
		this.NO3=NO3;
		
		this.constructSolution = new ConstructSolution(data);
		this.localImprovement = new LocalImprovement(data);
		
		this.rnd = new Random();
	}

	private Data data;
	private int NP;
	private double pls;
	private Random rnd;
	private long tmax;
	private int limit;
	private IConstructSolution constructSolution;
	private ILocalImprovement localImprovement;
	private int nd;
	private boolean NO1;
	private boolean NO2;
	private boolean NO3;
	
	public IGenerateNeighbouring newNO(){
		return new NO(data, NO1, NO2, NO3, nd);
	}
	
	public BestSolution exec() throws InterruptedException {
		Solution[] S = new Solution[NP];
		int[] noChanges = new int[NP];
		double[] fitness = new double[NP];
		
		BestSolution bestSol = new BestSolution();
		
		Thread[] employedBees = new Thread[NP];
		Thread[] onlookerBees = new Thread[NP];
		Thread[] scoutBees    = new Thread[NP];
		
		
		// Initialization phase
//		System.out.println("initialization");
		
		for(int i=0; i<NP ; i++) {
			S[i] = constructSolution.exec();
			if(rnd.nextDouble() < pls)
				S[i] = localImprovement.exec(S[i]);
			fitness[i] = S[i].getFitnessValue(data);
			employedBees[i] = new Thread(new EmployedBee(data, bestSol, i, S, noChanges, fitness, pls, newNO(),tmax));
			onlookerBees[i] = new Thread(new OnlookerBee(data, bestSol, S, noChanges, fitness, i, newNO(),tmax));
			scoutBees[i]= new Thread(new ScoutBee(data, bestSol, i, S, noChanges, fitness, limit, pls,tmax));
		}

		for(Thread bee : employedBees)
			bee.start();

		for(Thread bee : onlookerBees)
			bee.start();

		for(Thread bee : scoutBees)
			bee.start();
		
		


		for(Thread bee : employedBees)
			bee.join(tmax);

		for(Thread bee : onlookerBees)
			bee.join(tmax);

		for(Thread bee : scoutBees)
			bee.join(tmax);
		
		
		return bestSol;
	}

}
