package parallel;

import abc.Solution;

public class BestSolution {

	
	
	
	private Solution sol = null;
	private double fitness = 0;
	
	public double getFitness() {
		return fitness;
	}
	
	public Solution getSolution() {
		return sol;
	}
	
	public synchronized void setBestSolution(double fitness, Solution sol){
		if(fitness>this.fitness){
			this.fitness=fitness;
			this.sol=sol;
		}
	}
}
