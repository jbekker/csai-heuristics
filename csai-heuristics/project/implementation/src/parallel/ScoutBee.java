package parallel;

import java.util.Random;

import abc.ConstructSolution;
import abc.Data;
import abc.IConstructSolution;
import abc.ILocalImprovement;
import abc.LocalImprovement;
import abc.Solution;

public class ScoutBee implements Runnable {
	
	public ScoutBee(Data data, BestSolution bestSol, int i, Solution[] S, int[] noChanges, double[] fitness, int limit, double pls, long tmax){
		this.i=i;
		this.S=S;
		this.noChanges=noChanges;
		this.limit=limit;
		this.fitness = fitness;
		this.pls=pls;
		this.bestSol = bestSol;
		this.tmax=tmax;
		
		this.constructSolution = new ConstructSolution(data);
		this.localImprovement = new LocalImprovement(data);
	}
	
	private long tmax;
	private BestSolution bestSol;
	private int i;
	private Solution[] S;
	private int[] noChanges;
	private double[] fitness;
	private int limit;
	private double pls;
	private IConstructSolution constructSolution;
	private ILocalImprovement localImprovement;
	private Random rnd = new Random();
	
	@Override
	public void run() {
		long tinit = System.currentTimeMillis();
		while(System.currentTimeMillis()-tinit < tmax){
			if(noChanges[i]>limit){ //if Si does not change for limit operations
				S[i] = constructSolution.exec();
				if(rnd.nextDouble()>pls)
					S[i] = localImprovement.exec(S[i]); 
				if(fitness[i]>bestSol.getFitness()){
					bestSol.setBestSolution(fitness[i], S[i]);
				}
			}
		}

	}

}
