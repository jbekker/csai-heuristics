package parallel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import abc.BinaryTournament;
import abc.Data;
import abc.IBinaryTournament;
import abc.IGenerateNeighbouring;
import abc.ILocalImprovement;
import abc.LocalImprovement;
import abc.Solution;

public class OnlookerBee implements Runnable {
	
	public OnlookerBee(Data data, BestSolution bestSol, Solution[] S, int[] noChanges, double[] fitness, double pls, IGenerateNeighbouring NO, long tmax) {
		this.data=data;
		this.bestSol=bestSol;
		this.S=S;
		this.noChanges=noChanges;
		this.fitness=fitness;
		this.pls = pls;
		this.NO=NO;
		this.binaryTournament=new BinaryTournament(data);
		this.localImprovement=new LocalImprovement(data);
		this.tmax=tmax;
	}
	
	private long tmax;
	private BestSolution bestSol;
	private Data data;
	private Solution[] S;
	private int[] noChanges;
	private double[] fitness;
	private double pls;
	private IBinaryTournament binaryTournament;
	private ILocalImprovement localImprovement;
	private IGenerateNeighbouring NO;
	private Random rnd = new Random();

	@Override
	public void run() {
		long tinit = System.currentTimeMillis();
		while(System.currentTimeMillis()-tinit<tmax){
			int j = binaryTournament.exec(new ArrayList<Solution>(Arrays.asList(S)));
			Solution tempSol = NO.exec(S[j]);
			if(rnd.nextDouble() < pls)
				tempSol = localImprovement.exec( tempSol);
			double tempFitness = tempSol.getFitnessValue(data);
			if(tempFitness > fitness[j]){
				S[j]=tempSol;
				fitness[j] = tempFitness;
				noChanges[j]=0;
				if(fitness[j]>bestSol.getFitness())
					bestSol.setBestSolution(fitness[j], S[j]);
			}
		}
	}

}
