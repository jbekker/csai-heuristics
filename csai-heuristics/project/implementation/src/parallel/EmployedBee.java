package parallel;

import java.util.Random;

import abc.Data;
import abc.IGenerateNeighbouring;
import abc.ILocalImprovement;
import abc.LocalImprovement;
import abc.Solution;

public class EmployedBee implements Runnable {
	
	public EmployedBee(Data data, BestSolution bestSol, int i, Solution[] S, int[] NoChanges, double[] fitness, double pls, IGenerateNeighbouring NO, long tmax){
		this.tmax=tmax;
		this.i=i;
		this.S=S;
		this.noChanges=NoChanges;
		this.fitness=fitness;
		this.data=data;
		this.pls = pls;
		this.NO=NO;
		
		this.data = data;
		this.bestSol = bestSol;
		
		this.localImprovement = new LocalImprovement(data);
	}
			
	private long tmax;
	private BestSolution bestSol;		
	private Data data;
	private int i;
	private Solution[] S;
	private int[] noChanges;
	private double[] fitness;
	private double pls;
	private IGenerateNeighbouring NO;
	private ILocalImprovement localImprovement;
	private Random rnd = new Random();
	

	@Override
	public void run() {
		long tinit = System.currentTimeMillis();
		while(System.currentTimeMillis()-tinit<tmax){
			noChanges[i] = noChanges[i] +1; //new round -> one more round in which solution has not changed;
			Solution tempSol = NO.exec(S[i]);
			if(rnd.nextDouble() < pls)
				tempSol = localImprovement.exec(tempSol);
			double tempFitness = tempSol.getFitnessValue(data);
			if(tempFitness > fitness[i]){
				S[i]=tempSol;
				fitness[i] = tempFitness;
				noChanges[i]=0;
				if(fitness[i]>bestSol.getFitness())
					bestSol.setBestSolution(fitness[i], S[i]);
			}
		}
	}
}
