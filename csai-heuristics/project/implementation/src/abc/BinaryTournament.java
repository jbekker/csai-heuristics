package abc;
import java.util.ArrayList;
import java.util.Random;

public class BinaryTournament implements IBinaryTournament{
	
	public BinaryTournament(Data data) {
		this.data = data;
	}
	
	private Data data;

	@Override
	public int exec(ArrayList<Solution> sols) {
		// Check whether there are at least two solutions
		if (sols.size() < 2)
			throw new IllegalArgumentException("Less than 2 solutions");
		
		// Take a random first solution
		int i1 = new Random().nextInt(sols.size());
		
		// Take a random second solution that is not the first
		int i2 = i1;
		while (i1==i2) 
			i2 = new Random().nextInt(sols.size());
		// Alternative: shuffle list and take first two
		//		or	copy list, remove first and randomly take second
		
		// Compare the solutions and return the fittest solution
		if (sols.get(i1).getFitnessValue(data) >= sols.get(i2).getFitnessValue(data))
			return i1;
		else
			return i2;
	}

}
