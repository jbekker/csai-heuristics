package abc;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class NO2 implements IGenerateNeighbouring {
	
	public NO2(Data data, int nd){
		this.data = data;
		this.nd = nd;
	}
	
	private Data data;
	private int nd;

	@Override
	public Solution exec(Solution posSol) {
		Random rnd = new Random();
		int nbToRemove = rnd.nextInt(nd)+1;
		Solution incompleteSolution = posSol;
		
		Set<Integer> removedElements = new HashSet<Integer>();
				
		for(int i=0 ; i<nbToRemove ; i++){
			int group = rnd.nextInt(data.getNbGroups());
			while(incompleteSolution.getGroup(group).size()<2)
				group = rnd.nextInt(data.getNbGroups());
			int elementToRemove = incompleteSolution.selectWorstElement(group, data);
			removedElements.add(elementToRemove);
			incompleteSolution = incompleteSolution.removeElement(elementToRemove, data);
		}
		
		return incompleteSolution.completeSolution(removedElements, data);
	}

}