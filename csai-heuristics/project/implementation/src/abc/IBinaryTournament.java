package abc;
import java.util.ArrayList;

public interface IBinaryTournament {
	
	public int exec(ArrayList<Solution> sols);

}
