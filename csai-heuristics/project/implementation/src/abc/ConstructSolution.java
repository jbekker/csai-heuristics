package abc;
import java.util.Collections;
import java.util.ArrayList;



public class ConstructSolution implements IConstructSolution{
	
	public ConstructSolution(Data data){
		this.data = data;
	}
	
	private Data data;

	@Override
	public Solution exec() {
		ArrayList<Integer> elements = new ArrayList<>();
		for (int i=0; i < data.getNbElements(); i++)
			elements.add(i);
		// initialize solution with m empty groups
		Solution solution = new Solution(data.getNbGroups());
		
		// add 1 random element to each group
		Collections.shuffle(elements);
		for (int g=0; g < data.getNbGroups(); g++)
			solution = solution.addElementTo(elements.remove(0), g, data);
		
		solution = solution.completeSolution(elements, data);
		
		return solution;
	}
	
}
