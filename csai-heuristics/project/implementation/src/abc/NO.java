package abc;

import java.util.ArrayList;
import java.util.Random;

public class NO implements IGenerateNeighbouring {

	public NO(Data data, boolean NO1, boolean NO2, boolean NO3, int nd){
		if(NO1)
			NOs.add(new NO1(data, nd));
		if(NO2)
			NOs.add(new NO2(data, nd));
		if(NO3)
			NOs.add(new NO3(data, nd));
		nbNOs=NOs.size();

		if (nbNOs==0)
			throw new IllegalArgumentException("at least 1 NO necessary!");
		
		this.rnd = new Random();		
	}
	
	private ArrayList<IGenerateNeighbouring> NOs = new ArrayList<IGenerateNeighbouring>();
	private int nbNOs;
	private Random rnd;
	
	@Override
	public Solution exec(Solution posSol) {
		return NOs.get(rnd.nextInt(nbNOs)).exec(posSol);
	}

}
