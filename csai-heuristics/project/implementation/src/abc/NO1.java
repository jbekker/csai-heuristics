package abc;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class NO1 implements IGenerateNeighbouring{
	
	public NO1(Data data, int nd){
		this.data = data;
		this.nd = nd;
	}
	
	private Data data;
	private int nd;

	@Override
	public Solution exec(Solution posSol) {
		Random rnd = new Random();
		int nbToRemove = rnd.nextInt(nd)+1;
		Solution incompleteSolution = posSol;
		
		Set<Integer> removedElements = new HashSet<Integer>();
		
		for(int i=0 ; i<nbToRemove ; i++){
			int elementToRemove =  rnd.nextInt(data.getNbElements());
			while(removedElements.contains(elementToRemove) || incompleteSolution.getGroupOf(elementToRemove).size()<2)
				elementToRemove =  rnd.nextInt(data.getNbElements());
			removedElements.add(elementToRemove);
			incompleteSolution = incompleteSolution.removeElement(elementToRemove, data);
		}
		
		return incompleteSolution.completeSolution(removedElements, data);
	}

}
