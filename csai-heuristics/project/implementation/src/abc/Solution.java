package abc;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.HashSet;


public class Solution {
	
	/**
	 * Create solution with empty groups
	 * 
	 * @param nbOfGroups the number of different groups
	 */
	@SuppressWarnings("unchecked")
	public Solution(int nbOfGroups){
		this.nbOfGroups = nbOfGroups;
		groups = new Set[nbOfGroups];
		incompleteGroups = new HashSet<Integer>();
		for(int g=0 ; g<nbOfGroups ; g++) {
			groups[g] = new HashSet<Integer>();
			incompleteGroups.add(g);
		}
		unsatiatedGroups = new HashSet<Integer>(incompleteGroups);
	}
	
	private Solution(int nbOfGroups, Set<Integer>[] groups, 
			Set<Integer> incompleteGroups, Set<Integer> unsatiatedGroups) {
		this.nbOfGroups = nbOfGroups;
		this.groups = groups;
		this.incompleteGroups = incompleteGroups;
		this.unsatiatedGroups = unsatiatedGroups;
	}
	
	private final int nbOfGroups;
	private final Set<Integer>[] groups;
	// A set of all groups that have less than a_g elements
	private final Set<Integer> incompleteGroups; 
	// A set of all groups that have lass than b_g elements
	private final Set<Integer> unsatiatedGroups;
	
	@SuppressWarnings("unchecked")
	private Set<Integer>[] cloneGroups() {
		Set<Integer>[] newGroups = new Set[nbOfGroups];
		for (int g=0; g<nbOfGroups ; g++)
			newGroups[g] = getGroup(g);
		return newGroups;
	}
	/**
	 * Copy the groups array, making a copy of the group $group
	 * 
	 * @param group
	 * @return
	 */
	private Set<Integer>[] cloneGroups(int group) {
		Set<Integer>[] newGroups = groups.clone();
		newGroups[group] = new HashSet<Integer>(groups[group]);
		return newGroups;
	}
	
	@Override
	public Solution clone(){
		Set<Integer>[] newGroups = cloneGroups();
		Set<Integer> newIncompleteGroups = new HashSet<Integer> (incompleteGroups);
		Set<Integer> newUnsatiatedGroups = new HashSet<Integer> (unsatiatedGroups);
		
		return new Solution(nbOfGroups,newGroups,newIncompleteGroups, newUnsatiatedGroups);
	}
	
	public Solution addElementTo(int element, int group, Data data){
		
		Set<Integer>[] newGroups = cloneGroups(group);
		newGroups[group].add(element);
		Set<Integer> newIncompleteGroups = new HashSet<Integer> (incompleteGroups);
		Set<Integer> newUnsatiatedGroups = new HashSet<Integer> (unsatiatedGroups);

		if (newGroups[group].size() >= data.getLowerBound(group))
			newIncompleteGroups.remove(group);
		
		if (newGroups[group].size() >= data.getUpperBound(group))
			newUnsatiatedGroups.remove(group);
		
		return new Solution(nbOfGroups, newGroups, newIncompleteGroups, newUnsatiatedGroups);
		
		/*
		Solution newSol = clone();
		newSol.groups[group].add(element);
		
		if(newSol.groups[group].size() >= data.getLowerBound(group))
			newSol.incompleteGroups.remove(group);
		if(newSol.groups[group].size() >= data.getUpperBound(group))
			newSol.unsatiatedGroups.remove(group);
		
		return newSol;
		*/
	}
	
	public Solution removeElementFrom(int element, int group, Data data) {
		if (!groups[group].contains(element))
			throw new IllegalArgumentException("Group does not contain element");
		
		Set<Integer>[] newGroups = cloneGroups(group);
		newGroups[group].remove(element);
		Set<Integer> newIncompleteGroups = new HashSet<Integer> (incompleteGroups);
		Set<Integer> newUnsatiatedGroups = new HashSet<Integer> (unsatiatedGroups);
		
		newGroups[group].remove(element);
		if (newGroups[group].size() < data.getLowerBound(group))
			newIncompleteGroups.add(group);
		if (newGroups[group].size() < data.getUpperBound(group))
			newUnsatiatedGroups.add(group);
		return new Solution(nbOfGroups, newGroups, newIncompleteGroups, newUnsatiatedGroups);
		
		/*
		Solution newSol = clone();
		newSol.groups[group].remove(element);
		
		if(newSol.groups[group].size() < data.getLowerBound(group))
			newSol.incompleteGroups.add(group);
		if(newSol.groups[group].size() < data.getUpperBound(group))
			newSol.unsatiatedGroups.add(group);
		
		return newSol;
		*/
	}
	
	public Set<Integer> getGroup(int group) {
		return new HashSet<Integer>(groups[group]);
	}
	
	public int getGroupSize(int group) {
		return groups[group].size();
	}
	
	/*
	public Set<Integer>[] getGroups() {
		return groups;
	}
	*/
	
	public Solution removeElement(int elementToRemove, Data data) {
		
		return removeElementFrom(elementToRemove, getGroupNbOf(elementToRemove), data);
	}

	public double getFitnessValue(Data data) {
		double result = 0;
		for (Set<Integer> group : groups) {
			for (int i : group)
				result+=data.getValueContribution(i, group);
		}
		return result/2;
	}
	
	public Set<Integer> getIncompleteGroups() {
		return new HashSet<Integer>(incompleteGroups);
	}
	
	public Set<Integer> getUnsatiatedGroups() {
		return new HashSet<Integer>(unsatiatedGroups);
	}

	public Solution completeSolution(Collection<Integer> freeElements, Data data) {
		Solution solution  = this;
		ArrayList<Integer> elements = new ArrayList<>(freeElements);
		// while there are group with less then their min elements, select one of those groups at random
		// and add the element that maximizes this group's dissimilarity
		while(!solution.getIncompleteGroups().isEmpty()) {
			for (int g : solution.getIncompleteGroups()) {
				if (elements.isEmpty()) {
					System.out.println(solution);
					System.out.println(g + ": " + data.getLowerBound(g) + "-" + data.getUpperBound(g));
					throw new IllegalArgumentException("Too few elements");
				}
				int i = data.takeBestElement(elements, groups[g]);
				solution = solution.addElementTo(i, g, data);
			}
		}
		// while there are elements left, select a group at random which still has room
		// and add the element that maximizes this group's dissimilarity
		while(!elements.isEmpty()) {
			if (solution.getUnsatiatedGroups().isEmpty()) {
				System.out.println(solution);
				System.out.println(elements);
				throw new IllegalArgumentException("Too many elements");
			}
			for (int g : solution.getUnsatiatedGroups()) {
				int i = data.takeBestElement(elements, groups[g]);
				solution = solution.addElementTo(i, g, data);
				if (elements.isEmpty())
					break;
			}
		}
		
		return solution;
	}

	public int selectWorstElement(int group, Data data) {
		int worstElement = 0;
		double worstValue = Integer.MAX_VALUE;
		for (int element : groups[group]) {
			double value = data.getValueContribution(element, groups[group]);
			if (value < worstValue) {
				worstElement = element;
				worstValue = value;
			}
		}
			
		return worstElement;
	}
	
	public int getNbElements() {
		int nbElements = 0;
		for (Set<Integer> group : groups)
			nbElements += group.size();
		
		return nbElements;
	}
	
	public boolean isCompleteSolution(Data data) {
		for (int g=0; g < nbOfGroups; g++) {
			if (groups[g].size() > data.getUpperBound(g) ||
					groups[g].size() < data.getLowerBound(g))
				return false;
		}
		
		if (getNbElements() != data.getNbElements())
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		String result = "{";
		for (int g=0; g < nbOfGroups; g++) {
			result += g + " : " + groups[g].toString() + ", ";
		}
		result = result.substring(0, result.length()-2) + "}";
		return result;
	}
	
	public double getDiversityContribution(int element, int group, Data data){
		return data.getValueContribution(element, groups[group]);
	}

	public int getGroupNbOf(int element) {
		int group = -1;
		for (int g=0; g < nbOfGroups && group==-1; g++)
			if (groups[g].contains(element))
				group = g;
		return group;
	}
	
	public Set<Integer> getGroupOf(int element){
		return getGroup(getGroupNbOf(element));
	}

}
