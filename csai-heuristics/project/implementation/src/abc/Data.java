package abc;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Set;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;


public class Data {
	
	public Data(String path) throws IOException{
		parse(path);
	}
	
	private int nbGroups;
	private int nbElements;
	private int[][] boundaries;
	private double[][] dissimilarities;
	
	public double getDissimilarity(int i, int j) {
		return dissimilarities[i][j];
	}
	
	public double getValueContribution(int element, Set<Integer> group) {
		double result = 0;
		for (int j : group)
			if (j != element)
				result += getDissimilarity(element, j);
		return result/2;
	}
	
	public int takeBestElement(Collection<Integer> candidateElements, Set<Integer> group) {
		if (candidateElements.isEmpty() || group.isEmpty())
			throw new IllegalArgumentException();
		int bestElement = -1;
		double bestValue = 0;
		for (int i : candidateElements) {
			double value = getValueContribution(i, group);
			if (value >= bestValue) {
				bestElement = i;
				bestValue = value;
			}
		}
		candidateElements.remove(bestElement);
		if (bestElement == -1)
			throw new IllegalStateException();
		return bestElement;
	}
	
	public int getNbElements() {
		return nbElements;
	}
	
	public int getNbGroups() {
		return nbGroups;
	}
	
	public int getLowerBound(int group) {
		return boundaries[group][0];
	}
	
	public int getUpperBound(int group) {
		return boundaries[group][1];
	}
	
	public boolean isSameSizeGroupProblem(){
		throw new NotImplementedException();
	}
	
	private void parse(String path) throws IOException{
		File file = new File(path);
		BufferedReader br = new BufferedReader(new FileReader(file));
		
		readInfoLine(br.readLine());
		for(String line; (line = br.readLine()) != null; ) {
		   readDissimilarityLine(line);
		}
		br.close();
	}
	
	private void readInfoLine(String line) {
		String[] tokens = line.split("\\s+");
		nbElements = Integer.parseInt(tokens[0]);
		nbGroups = Integer.parseInt(tokens[1]);
		
		boundaries = new int[nbGroups][2];
		for(int i=0 ; i<nbGroups ; i++){
			boundaries[i][0] = Integer.parseInt(tokens[3+i*2]);
			boundaries[i][1] = Integer.parseInt(tokens[3+i*2+1]);
		}
		
		dissimilarities = new double[nbElements][nbElements];
	}
	
	private void readDissimilarityLine(String line) {
		String[] tokens = line.split("\\s+");

		int i = Integer.parseInt(tokens[0]);
		int j = Integer.parseInt(tokens[1]);
		double d = Double.parseDouble(tokens[2]);
		
		dissimilarities[i][j] = d;
		dissimilarities[j][i] = d;
	}

	@Override
	public String toString() {
		return "data: G=" + getNbGroups() + ", E=" + getNbElements() + ", bounds=" + boundaries;
	}
}
