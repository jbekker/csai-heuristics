package abc;

import java.util.Random;
import java.util.Set;

public class NO3 implements IGenerateNeighbouring {
	
	public NO3(Data data, int p){
		this.data = data;
		this.p = p;
	}
	
	int p;
	private Data data;
	
	@Override
	public Solution exec(Solution posSol) {
		Random rnd = new Random();
		int swapsToPerform = rnd.nextInt(p)+1;
		
		for(int i=0 ; i<swapsToPerform ; i++){
			// choose groups to swap from
			int group1 = rnd.nextInt(data.getNbGroups());
			int group2 = rnd.nextInt(data.getNbGroups()-1);
			if(group1 == group2)
				group2 = data.getNbGroups()-1;

			int el1 = chooseRandomFromGroup(posSol.getGroup(group1));
			int el2 = chooseRandomFromGroup(posSol.getGroup(group2));
			
			posSol = posSol.removeElement(el1, data);
			posSol = posSol.removeElement(el2, data);
			posSol = posSol.addElementTo(el1, group2, data);
			posSol = posSol.addElementTo(el2, group1, data);
		}
		
		return posSol;
	}
	
	private int chooseRandomFromGroup(Set<Integer> group) {
		int size = group.size();
		int item = new Random().nextInt(size);
		int i = 0;
		for(Integer el : group)
		{
		    if (i == item)
		        return el;
		    i = i + 1;
		}
		return 0;
	}

}
