package abc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class ABC {
	public static boolean debug = false;
	
	public ABC(Data data, int NP, double pls, double tmax, float limit_factor, float nd_factor, boolean NO1, boolean NO2, boolean NO3) {
		this.data = data;
		setNP(NP);
		setPls(pls);
		setTmax(tmax);
		setLimit(limit_factor);
		
		this.binaryTournament = new BinaryTournament(data);
		this.constructSolution = new ConstructSolution(data);
		this.localImprovement = new LocalImprovement(data);
		
		this.rnd = new Random();
		
		this.NO1 = NO1;		
		this.NO2 = NO2;		
		this.NO3 = NO3;
		setNd(nd_factor);
	}

	private Data data;
	private int NP;
	private double pls;
	private Random rnd;
	private double tmax;
	private double tinit;
	private int limit;
	private IBinaryTournament binaryTournament;
	private IConstructSolution constructSolution;
//	private ArrayList<IGenerateNeighbouring> NOs;
	private ILocalImprovement localImprovement;
//	private int nbNOs;
	private int nd;
	private boolean NO1;
	private boolean NO2;
	private boolean NO3;
	private IGenerateNeighbouring NO;
	
	public void setNOs(boolean NO1, boolean NO2, boolean NO3){
		/*NOs = new ArrayList<IGenerateNeighbouring>();
		if(NO1){
			NOs.add(new NO1(data, nd));
		}
		if(NO2){
			NOs.add(new NO2(data, nd));
		}
		if(NO3){
			NOs.add(new NO3(data, nd));
		}
		nbNOs = NOs.size();
		if (nbNOs==0)
			throw new IllegalArgumentException("at least 1 NO necessary!");*/
		this.NO=new NO(data, NO1, NO2, NO3, nd);
	}
	
	public void setNd(float nd_factor){
		this.nd = Math.round(nd_factor * data.getNbElements());
		setNOs(NO1, NO2, NO3);
	}
	
	public void setNP(int NP){
		this.NP = NP;
	}
	
	public void setLimit(float limit_factor){
		this.limit=Math.round(limit_factor * data.getNbElements());
	}
	
	public void setTmax(double tmax){
		this.tmax=tmax*1000;
	}
	
	public void setPls(double pls2){
		this.pls=pls2;
	}
	
	
	
//	private Solution NOexec(Solution posSol) {
//		return NOs.get(rnd.nextInt(nbNOs)).exec(posSol);
//	}
	
	public Solution exec() {

		Solution[] S = new Solution[NP];
		int[] noChanges = new int[NP];
		double[] fitness = new double[NP];
		
		Solution tempSol;
		double tempFitness;

		Solution bestSolution = null;
		double bestSolutionFitness = 0;
		
		
		// Initialization phase
		if (debug)
			System.out.println("initialization");
		
		for(int i=0; i<NP ; i++) {
			S[i] = constructSolution.exec();
			if(rnd.nextDouble() < pls)
				S[i] = localImprovement.exec(S[i]);
			fitness[i] = S[i].getFitnessValue(data);
		}

		tinit = System.currentTimeMillis();
		while(System.currentTimeMillis()-tinit < tmax) { //computation tima tmax not reached
			if (debug)
				System.out.println("new round");
			
			// Employed bees phase
			if (debug)
				System.out.println("employed bees");
			for(int i = 0; i<NP ; i++){
				noChanges[i] = noChanges[i] +1; //new round -> one more round in which solution has not changed;
				tempSol = NO.exec(S[i]);
				if(rnd.nextDouble() < pls)
					tempSol = localImprovement.exec(tempSol);
				tempFitness = tempSol.getFitnessValue(data);
				if(tempFitness > fitness[i]){
					S[i]=tempSol;
					fitness[i] = tempFitness;
					noChanges[i]=0;
				}
			}
			
			
			// Onlooker bees phase
			if (debug)
				System.out.println("onlooker bees");
			
			for(int i=0 ; i<NP ; i++){
				int j = binaryTournament.exec(new ArrayList<Solution>(Arrays.asList(S)));
				tempSol = NO.exec(S[j]);
				if(rnd.nextDouble() < pls)
					tempSol = localImprovement.exec( tempSol);
				tempFitness = tempSol.getFitnessValue(data);
				if(tempFitness > fitness[j]){
					S[j]=tempSol;
					fitness[j] = tempFitness;
					noChanges[j]=0;
				}
			}
			
			
			// Scout bees phase
			if (debug)
				System.out.println("scout bees");
			
			for(int i=0 ; i<NP ; i++){
				if(noChanges[i]>limit){ //if Si does not change for limit operations
					S[i] = constructSolution.exec();
					if(rnd.nextDouble()>pls)
						S[i] = localImprovement.exec(S[i]); 
				}
			}
			
			
			// Remember the best found source so far
			if (debug)
				System.out.println("remember best");
			
			for(int i=0 ; i<NP ; i++) {
				if(fitness[i]>bestSolutionFitness){
					bestSolution = S[i];
					bestSolutionFitness = fitness[i];
				}
			}
		}
//		System.out.println(r);
		return bestSolution;
	}

}
