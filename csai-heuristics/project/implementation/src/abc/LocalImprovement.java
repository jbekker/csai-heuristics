package abc;

import java.util.ArrayList;
import java.util.List;

import tools.Pair;

public class LocalImprovement implements ILocalImprovement {

	public LocalImprovement(Data data){
		this.data = data;
	}
	
	private Data data;
	
	@Override
	public Solution exec(Solution sol) {
		boolean hasImproved = true;
		double improvement = 0;
		
		List<Integer> possibleGroupsFrom = new ArrayList<Integer>();
		List<Integer> possibleGroupsTo = new ArrayList<Integer>();
		for(int group=0 ; group<data.getNbGroups() ; group++){
			int elementsInGroup = sol.getGroupSize(group);
			if(elementsInGroup>data.getLowerBound(group))
				possibleGroupsFrom.add(group);
			if(elementsInGroup<data.getUpperBound(group))
				possibleGroupsTo.add(group);
		}
		
		
		while(hasImproved){
			hasImproved = false;
			Pair<Double, Solution> effect;
			effect = swap(sol, improvement);
			effect = move(effect.right, effect.left, possibleGroupsFrom, possibleGroupsTo);
			if(effect.left>0){
				hasImproved = true;
				sol = effect.right;
			}
		}
		
		
		return sol;
	}
	
	
	private Pair<Double, Solution> move(Solution sol, double improvement, List<Integer> possibleGroupsFrom, List<Integer> possibleGroupsTo) {
		List<Integer> groups1 = new ArrayList<>(possibleGroupsFrom);
		List<Integer> groups2 = new ArrayList<>(possibleGroupsTo);
		
		double d1;
		double d;
		looplbl: for(int group1: groups1){
			for(int el: sol.getGroup(group1)){
				d1 = sol.getDiversityContribution(el, group1, data);
				for(int group2: groups2){
					d = sol.getDiversityContribution(el, group2, data) - d1;
					if(d>0){
						improvement+=d;
						sol = sol.removeElementFrom(el, group1, data);
						sol = sol.addElementTo(el, group2, data);
						
						if(sol.getGroup(group1).size()==data.getLowerBound(group1))
							possibleGroupsFrom.remove(new Integer(group1));
						if(!possibleGroupsTo.contains(group1))
							possibleGroupsTo.add(group1);
						
						if(sol.getGroup(group2).size()==data.getUpperBound(group2))
							possibleGroupsTo.remove(new Integer(group2));
						if(!possibleGroupsFrom.contains(group2))
							possibleGroupsFrom.add(group2);
						
						break looplbl;
					}
				}	
			}
		}
		return new Pair<Double,Solution>(improvement, sol);
	}

	private Pair<Double, Solution> swap(Solution sol, double improvement) {
		double D11;
		double D12;
		double D21;
		double D22;
		double d12;
		double delta;
		looplbl: for(int group1=0 ; group1 < data.getNbGroups() ; group1++){
			for(int el1: sol.getGroup(group1)){
				D11 = sol.getDiversityContribution(el1, group1, data);
				for(int group2=0 ; group2 < data.getNbGroups() ; group2++){
					if(group1!=group2){
						D21 = sol.getDiversityContribution(el1, group2, data);
						for(int el2 : sol.getGroup(group2)){
							D12 = sol.getDiversityContribution(el2, group1, data);
							D22 = sol.getDiversityContribution(el2, group2, data);
							d12 = data.getDissimilarity(el1, el2);
							delta = D12 + D21 - D11 - D22 -2*d12;
							if(delta>0){
								improvement+=delta;
								sol = sol.removeElementFrom(el1, group1, data);
								sol = sol.removeElementFrom(el2, group2, data);
								sol = sol.addElementTo(el1, group2, data);
								sol = sol.addElementTo(el2, group1, data);
								
								break looplbl;
							}
						}
					}
				}	
			}
		}
		return new Pair<Double,Solution>(improvement, sol);
	}

}
